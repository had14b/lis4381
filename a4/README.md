# LIS4381 - Mobile Web Application Development

## Homer A Davis

### Assignment 4 Requirements:

*Four Parts:*

1. Clone starter files from bitbucket
2. Edit index.php
3. Add jQuery validation and reg expressions
4. Must include favicon

#### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Screenshot of Home Page

#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Screenshot of Failed Validation](img/fail.PNG)

*Screenshot of Passed Validation*:

![Screenshot of Passed Validation](img/pass.PNG)

*Screenshot of Home Page*:

![Screenshot of Home Page](img/home.PNG)

#### Link to A4 on localhost:

[http://localhost/repos/lis4381/](http://localhost/repos/lis4381/)
