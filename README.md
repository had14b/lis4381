> **NOTE:** A README.md file should be placed at the **root of each of your repos directories.**
>


# LIS4381 - Mobile Web Application Development

## Homer Davis

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials
    * Provide git command descriptions 
2. [A2 README.md](a2/README.md)
    * Create Recipe App
    * App with working button
    * Add TextView to app
    * Add ImageView to app
    * Change text size
3. [A3 README.md](a3/README.md)
    * Create MyEvent App
    * Create ERD
    * Add data to tables
4. [A4 README.md](a4/README.md)
    * Clone starter files from bitbucket
    * Edit index.php
    * Add jQuery validation and reg expressions
5. [A5 README.md](a5/README.md)
    * Use A4 files
    * Edit PHP files
    * Include Error Message
6. [P1 README.md](p1/README.md)
    * Create BusinessCard App
    * Add screenshots
7. [P2 README.md](p2/README.md)
    * Add Edit funcitonality
    * Add Delete functionality
    