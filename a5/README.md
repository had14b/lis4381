# LIS4381 - Mobile Web Application Development

## Homer A Davis

### Assignment 5 Requirements:

*Four Parts:*

1. Clone starter files from bitbucket
2. Edit php files
3. Add validation
4. Must include favicon

#### README.md file should include the following items:

* Screenshot of Datatable
* Screenshot of Failed Validation Error

#### Assignment Screenshots:

*Screenshot of DataTable*:

![Screenshot of Failed Validation](img/table.PNG)

*Screenshot of Validation Error*:

![Screenshot of Failed Validation](img/error.PNG)


#### Link to A4 on localhost:

[http://localhost/repos/lis4381/](http://localhost/repos/lis4381/)
