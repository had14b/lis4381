# LIS4381 - Mobile Web Application Development

## Homer A Davis

### Project 1 Requirements:

*Five Parts:*

1. Backwards engineer screenshots of application
2. Create a launcher icon image and display it in both activities (screens)
3. Must add background color(s) to both activities
4. Must add border around image and button
5. Must add text shadow (button)

#### README.md file should include the following items:

* Screenshot of Screen 1
* Screenshot of Screen 2

#### Assignment Screenshots:

|*Screenshot of Screen 1*|*Screenshot of Screen 2*|
-------------------------------|--------------------------------|
|![Business Card Profile screenshot](img/screen1.PNG)|![Interests screenshot](img/screen2.PNG)|

