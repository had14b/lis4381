# LIS4381 - Mobile Web Application Development

## Homer A Davis

### Assignment 5 Requirements:

*Four Parts:*

1. Clone starter files from bitbucket
2. Edit php files
3. Add validation
4. Must include favicon

#### README.md file should include the following items:

* Screenshot of Data Screen
* Screenshot of Edit Screen
* Screenshot of Failed Validation Error
* Screenshot of RSS Feed

#### Assignment Screenshots:

*Screenshot of Data Screen*:

![Screenshot of Data Validation](img/index.PNG)

*Screenshot of Edit Screen*:

![Screenshot of Failed Validation](img/edit.PNG)

*Screenshot of Validation Error*:

![Screenshot of Failed Validation](img/error.PNG)

*Screenshot of RSS Screen*:

![Screenshot of RSS Feed](img/rss.PNG)


#### Link to A4 on localhost:

[http://localhost/repos/lis4381/](http://localhost/repos/lis4381/)
