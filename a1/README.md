> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Homer Davis

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter 1 & 2 Questions
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi "My PHP Installation")
* Screenshot of running Java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one
2. git status: Show the working tree status
3. git add: Add file contents to the index
4. git commit: Record changes to the repository
5. git push: Update remote refs along with associated objects
6. git pull: Fetch from and integrate with another repository or a local branch
7. git clone: Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi "My PHP Installation")*: 

![AMPPS running](img/php.PNG)

*Screenshot of running Java Hello*:

![Screenshot of running Java Hello](img/java.PNG)

*Screenshot of Android Studio - My First App*:

![Screenshot of Android Studio - My First App](img/myFirstApp.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/had14b/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/had14b/myteamquotes/ "My Team Quotes Tutorial")
