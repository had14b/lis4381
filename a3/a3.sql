-- MySQL Script generated by MySQL Workbench
-- Mon Oct 22 21:08:49 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema had14b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `had14b` ;

-- -----------------------------------------------------
-- Schema had14b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `had14b` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `had14b` ;

-- -----------------------------------------------------
-- Table `had14b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `had14b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `had14b`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`),
  UNIQUE INDEX `pst_id_UNIQUE` (`pst_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `had14b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `had14b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `had14b`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`),
  UNIQUE INDEX `cus_id_UNIQUE` (`cus_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `had14b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `had14b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `had14b`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  UNIQUE INDEX `pet_id_UNIQUE` (`pet_id` ASC),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `had14b`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `had14b`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `had14b`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `had14b`;
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (1, 'Pet Smart', '123 Main St', 'Panama City', 'FL', 32401, 8508992034, 'petsmart@gmail.com', 'petsmart.com', 345266.37, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (2, 'Pets Mart', '345 First St', 'Tallahassee', 'FL', 32304, 8503405539, 'petsmart@petsmart.com', 'petsmart.org', 83525.24, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (3, 'Pet Store', '749 Jackson Blvd', 'Tallahassee', 'FL', 32304, 8502538751, 'petstore@gmail.com', 'petstore.com', 98435.26, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (4, 'Pet Co', '830 Port Dr', 'Tallahassee', 'FL', 32304, 8509273523, 'petco@gmail.com', 'petco.com', 47856.23, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (5, 'The Pet Company', '293 Timber St', 'Panama City', 'FL', 32401, 8509739693, 'thepetcompany@gmail.com', 'thepetcompany.com', 346523.09, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (6, 'Raining Cats & Dogs', '932 Johnson Dr', 'Tallahassee', 'FL', 32304, 8509372246, 'rainingcats@gmail.com', 'rainingcatsanddogs.com', 23421.34, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (7, 'Who Let the Dogs Out', '604 MLK Blvd', 'Tallahassee', 'FL', 32304, 8509326883, 'wholetthedogsout@gmail.com', 'wholetthedogsout.com', 253654.74, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (8, 'We Love Pets', '362 King St', 'Panama City', 'FL', 32401, 8502293462, 'welovepets@gmail.com', 'welovepets.com', 23467.34, NULL);
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (9, 'Happy Paws', '8252 Tennessee St', 'Tallahassee', 'FL', 32304, 8509982756, 'happypaws@gmail.com', 'happypaws.com', 923562.93, 'Highest sales');
INSERT INTO `had14b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (10, 'Worlds Best Pets', '264 Window Ln', 'Tallahassee', 'FL', 32304, 8500859322, 'worldsbestpets@gmail.com', 'worldsbestpets.com', 346234.53, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `had14b`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `had14b`;
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (1, 'Allen', 'Davis', '1234 Main St', 'Tallahassee', 'FL', 32304, 8509992692, 'allen@davis.com', 0.00, 100.00, 'He\'s a pretty cool guy');
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (2, 'Josh', 'Mac', '627 Pine Dr', 'Panama City', 'FL', 32401, 8509992625, 'josh@mac.com', 0.00, 200.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (3, 'John', 'Doe', '273 Jordan St', 'Panama City', 'FL', 32401, 8509992523, 'john@doe.com', 0.00, 100.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (4, 'Jane', 'Does', '9983 Pensacola St', 'Tallahassee', 'FL', 32304, 8509995723, 'jane@does.com', 0.00, 100.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (5, 'Mike', 'Cross', '826 Johnson Dr', 'Tallahassee', 'FL', 32304, 8509932467, 'mike@cross.com', 0.00, 200.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (6, 'Landon', 'Tewers', '221 First St', 'Tallahassee', 'FL', 32304, 8502345823, 'landon@tewers.com', 0.00, 100.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (7, 'Garrett', 'Russell', '128 Jameson Blvd', 'Panama City', 'FL', 32401, 8509992362, 'garrett@russell.com', 0.00, 200.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (8, 'Garrett', 'Rapp', '293 Woodward Ave', 'Panama City', 'FL', 32401, 8509993846, 'garrett@rapp.com', 0.00, 100.00, 'Don\'t mess with this guy');
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (9, 'Dylan', 'Guarino', '849 Jiffy Dr', 'Tallahassee', 'FL', 32304, 8509999445, 'dylan@guarino.com', 0.00, 500.00, NULL);
INSERT INTO `had14b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (10, 'Tyler', 'Joseph', '921 Canada St', 'Panama City', 'FL', 32401, 8509999904, 'tyler@joseph.com', 0.00, 200.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `had14b`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `had14b`;
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, 1, 'Dog', 'm', 234.23, 300.00, 1, 'Black', '2018-02-10', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, 2, 'Dog', 'm', 252.63, 300.00, 1, 'Brown', '2018-01-03', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, 3, 'Bird', 'm', 733.73, 900.00, 1, 'Blue', '2018-01-23', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, 4, 'Cat', 'f', 327.32, 400.00, 2, 'White', '2018-02-12', 'y', 'y', 'Demon Possessed');
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, 5, 'Dog', 'f', 723.72, 850.00, 1, 'White', '2018-01-30', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, 6, 'Cat', 'm', 362.73, 450.00, 1, 'Gray', '2018-01-13', 'y', 'y', 'Evil');
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, 7, 'Snake', 'f', 263.73, 350.00, 2, 'Red', '2018-01-11', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, 8, 'Bird', 'm', 838.52, 950.00, 3, 'Green', '2018-02-02', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, 9, 'Hamster', 'm', 73.28, 100.00, 1, 'Brown', '2018-01-09', 'y', 'y', NULL);
INSERT INTO `had14b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, 10, 'Dog', 'f', 836.86, 950.00, 1, 'Black', '2018-02-20', 'y', 'y', NULL);

COMMIT;

