# LIS4381 - Mobile Web Application Development

## Homer A Davis

### Assignment 3 Requirements:

*Six Parts:*

1. Backwards engineer screenshot of application
2. Create a launcher icon image and display it in both activities (screens)
3. Must add background color(s) to both activities
4. Must add border around image and button
5. Must add text shadow (button)
6. Create Database

#### README.md file should include the following items:

* Screenshot of Screen 1
* Screenshot of Screen 2
* Screenshot of ERD

#### Assignment Screenshots:

|*Screenshot of Screen 1*|*Screenshot of Screen 2*|
-------------------------------|--------------------------------|
|![Unpopulated app screenshot](img/unpop.png)|![Populated app screenshot](img/pop.png)|

*Screenshot of ERD*:

![Screenshot of ERD](img/erd.png)

#### SQL Files:

[a3.sql](a3.sql)

[a3.mwb](a3.mwb)